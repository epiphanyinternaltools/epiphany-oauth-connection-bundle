<?php

namespace spec\Epiphany\OAuthConnectionBundle\Provider;

use Epiphany\OAuthConnectionBundle\Config\ProviderConfig;
use League\OAuth2\Client\Provider\AbstractProvider;
use League\OAuth2\Client\Provider\Google;
use PhpSpec\ObjectBehavior;
use Symfony\Component\Routing\Router;

class ProviderFactorySpec extends ObjectBehavior
{
    function let(Router $router)
    {
        $this->beConstructedWith($router);
    }
    function it_is_initializable()
    {
        $this->shouldHaveType('Epiphany\OAuthConnectionBundle\Provider\ProviderFactory');
    }
    function it_should_generate_the_redirect_url_and_then_insert_it_into_a_provider_and_return_it(Router $router,
                                                                                                  ProviderConfig $providerConfig)
    {
        $redirect = $router->generate('epiphany_oauth_connect.authorize', [], 0)
            ->willReturn('some URL')
        ;

        $providerConfig->getType()
            ->willReturn("generic")
            ;

        $providerConfig->getName()
            ->willReturn("the_client_name")
            ;

        $providerConfig->getClientId()
            ->willReturn("the_client_id")
        ;
        $providerConfig->getAuthorizeUrl()
            ->willReturn("authorize url")
        ;
        $providerConfig->getClientSecret()->
        willReturn("the_client_secret")
        ;
        $providerConfig->getTokenUrl()
            ->willReturn("the_token_url")
        ;
        $providerConfig->getResourceOwnerUrl()
            ->willReturn("the_resource_owner_url")
        ;
        $providerConfig->getScopes()
            ->willReturn(["the_account_scopes"])
            ;

        /**
         * @var AbstractProvider $provider
         */
        $provider = $this->getProvider($providerConfig);

        $provider->shouldHaveType(AbstractProvider::class);

    }

    function it_constructs_a_google_provider_if_the_type_is_google(Router $router,
                                                                   ProviderConfig $providerConfig)
    {
        $redirect = $router->generate('epiphany_oauth_connect.authorize', [], 0)
            ->willReturn('some URL')
        ;

        $providerConfig->getType()
            ->willReturn("google")
        ;

        $providerConfig->getName()
            ->willReturn("the_client_name")
        ;

        $providerConfig->getClientId()
            ->willReturn("the_client_id")
        ;
        $providerConfig->getAuthorizeUrl()
            ->willReturn("authorize url")
        ;
        $providerConfig->getClientSecret()->
        willReturn("the_client_secret")
        ;
        $providerConfig->getTokenUrl()
            ->willReturn("the_token_url")
        ;
        $providerConfig->getResourceOwnerUrl()
            ->willReturn("the_resource_owner_url")
        ;
        $providerConfig->getScopes()
            ->willReturn(["the_account_scopes"])
        ;

        /**
         * @var AbstractProvider $provider
         */
        $provider = $this->getProvider($providerConfig);

        $provider->shouldHaveType(Google::class);

    }
}
