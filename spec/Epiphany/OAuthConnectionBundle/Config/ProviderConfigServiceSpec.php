<?php

namespace spec\Epiphany\OAuthConnectionBundle\Config;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Epiphany\OAuthConnectionBundle\Config\Exception\ProviderConfigNotFoundException;
use Epiphany\OAuthConnectionBundle\Config\ProviderConfig;

class ProviderConfigServiceSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType('Epiphany\OAuthConnectionBundle\Config\ProviderConfigService');
    }
    function it_stores_provider_config_in_an_array_and_then_retrieves_it(
        ProviderConfig $providerConfig
    )
    {
        $providerConfig->getName()->willReturn('some key');
        $this->registerProviderConfig($providerConfig);
        $this->getProviderConfig('some key')
            ->shouldBe($providerConfig);
    }
    function it_throws_an_exception_if_the_config_is_not_found(
        ProviderConfig $providerConfig
    )
    {
        $providerConfig->getName()->willReturn('some key');
        $this->registerProviderConfig($providerConfig);
        $this->shouldThrow(ProviderConfigNotFoundException::class)
            ->duringGetProviderConfig('some other key')
        ;

    }
}
