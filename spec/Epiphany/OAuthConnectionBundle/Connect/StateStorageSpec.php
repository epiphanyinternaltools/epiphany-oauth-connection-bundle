<?php

namespace spec\Epiphany\OAuthConnectionBundle\Connect;

use Epiphany\OAuthConnectionBundle\Connect\Exception\StateNotFoundException;
use PhpSpec\ObjectBehavior;
use Symfony\Component\HttpFoundation\Session\Session;

class StateStorageSpec extends ObjectBehavior
{
    function let(Session $session)
    {
        $this->beConstructedWith($session);
    }
    function it_is_initializable()
    {
        $this->shouldHaveType('Epiphany\OAuthConnectionBundle\Connect\StateStorage');
    }
    function it_stores_an_account_key_in_session_using_state_as_a_key(Session $session)
    {

        $session->set('some state', 'some key')
            ->shouldBeCalled()
        ;

        $this->storeState('some state', 'some key');

    }

    function it_retrieves_an_account_key_based_on_state(Session $session)
    {

        $session->has('some state')->shouldBeCalled()
            ->willReturn(true)
        ;

        $session->get('some state')->shouldBeCalled()
            ->willReturn('some key')
        ;

        $this->getState('some state')
            ->shouldBe('some key')
        ;

    }

    function it_returns_an_error_because_state_does_not_exist(Session $session)
    {

        $session->has('some_other_state')->shouldBeCalled()
            ->willReturn(false)
        ;

        $this->shouldThrow(StateNotFoundException::class)
            ->duringGetState('some_other_state')
        ;

    }
}
