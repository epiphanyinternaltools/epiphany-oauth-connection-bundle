<?php

namespace spec\Epiphany\OAuthConnectionBundle\Connect;

use Epiphany\OAuthConnectionBundle\Account\Account;
use Epiphany\OAuthConnectionBundle\Config\ProviderConfig;
use Epiphany\OAuthConnectionBundle\Config\ProviderConfigService;
use Epiphany\OAuthConnectionBundle\Connect\StateStorage;
use Epiphany\OAuthConnectionBundle\Provider\ProviderFactory;
use League\OAuth2\Client\Grant\RefreshToken;
use League\OAuth2\Client\Provider\AbstractProvider;
use League\OAuth2\Client\Token\AccessToken;
use PhpSpec\ObjectBehavior;

class ConnectServiceSpec extends ObjectBehavior
{
    function let(
        ProviderConfigService $configService,
        ProviderFactory $factory,
        StateStorage $stateStorage
    )
    {
        $this->beConstructedWith($configService, $factory, $stateStorage);
    }
    function it_is_initializable()
    {
        $this->shouldHaveType('Epiphany\OAuthConnectionBundle\Connect\ConnectService');
    }
    function it_gets_an_authorization_url_from_an_account(
        Account $account,
        ProviderConfigService $configService,
        ProviderConfig $config,
        ProviderFactory $factory,
        AbstractProvider $abstractProvider,
        StateStorage $stateStorage
    )
    {

        $account->getKey()
            ->willReturn('acc_key');
        $account->getAccountType()
            ->willReturn('google');

        $configService->getProviderConfig('google')
            ->shouldBeCalled()
            ->willReturn($config);

        $config->getScopes()->willReturn(['a scope']);

        $factory->getProvider($config)
            ->shouldBeCalled()
            ->willReturn($abstractProvider)
        ;

        $abstractProvider->getState()
            ->willReturn('state')
        ;

        $stateStorage->storeState('state', 'acc_key')
            ->shouldBeCalled()
        ;

        $abstractProvider
            ->getAuthorizationUrl([
                'approval_prompt' => 'force',
                'scope' => ['a scope'],
            ])
            ->willReturn('auth_url')
        ;
        $this->getAuthorizationUrl($account)->shouldReturn('auth_url');

    }
    function it_gets_a_refresh_token_for_an_account(
        Account $account,
        ProviderConfigService $configService,
        ProviderConfig $config,
        ProviderFactory $factory,
        AbstractProvider $abstractProvider,
        AccessToken $token,
        RefreshToken $refreshToken
    )
    {
        $account->getAccountType()->willReturn('google');

        $configService->getProviderConfig('google')
            ->shouldBeCalled()
            ->willReturn($config)
        ;

        $factory->getProvider($config)->shouldBeCalled()
            ->willReturn($abstractProvider)
        ;
        $abstractProvider->getAccessToken(
            'authorization_code',
            ['code' => 'authorization_code_from_google']
        )
            ->willReturn($token)
        ;
        $token->getRefreshToken()->willReturn($refreshToken);
        $this->getRefreshToken($account, 'authorization_code_from_google')
            ->shouldReturn($refreshToken)
        ;
    }
    function it_gets_an_access_token_for_an_account(
        Account $account,
        ProviderConfigService $configService,
        ProviderConfig $config,
        ProviderFactory $factory,
        AbstractProvider $abstractProvider,
        AccessToken $accessToken
    )
    {
        $account->getRefreshToken()->willReturn('refresh');
        $account->getAccountType()->willReturn('google');

        $configService->getProviderConfig('google')
            ->shouldBeCalled()->willReturn($config)
        ;

        $factory->getProvider($config)
            ->shouldBeCalled()->willReturn($abstractProvider)
        ;

        $abstractProvider
            ->getAccessToken(
                'refresh_token',
                ['refresh_token' => 'refresh']
            )->willReturn($accessToken)
        ;

        $this->getAccessToken($account)->shouldReturn($accessToken);
    }
    function it_returns_null_if_the_account_has_no_refresh_token(
        Account $account
    )
    {
        $account->getRefreshToken()
            ->willReturn('')
        ;

        $this->getAccessToken($account)->shouldReturn(null);
    }
}
