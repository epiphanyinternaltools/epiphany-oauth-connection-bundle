<?php

namespace Epiphany\OAuthConnectionBundle\Config;

use Epiphany\OAuthConnectionBundle\Config\Exception\ProviderConfigNotFoundException;

class ProviderConfigService
{
    /** @var array  */
    private $configStore;

    public function __construct()
    {
        $this->configStore = [];
    }

    public function registerProviderConfig(ProviderConfig $providerConfig)
    {
        $providerName = $providerConfig->getName();
        $this->configStore[$providerName] = $providerConfig;
    }

    public function getProviderConfig($providerName):ProviderConfig
    {
        if (!isset($this->configStore[$providerName]))
        {
           throw new ProviderConfigNotFoundException("Could not find provider $providerName");
        }
        return $this->configStore[$providerName];

    }
}
