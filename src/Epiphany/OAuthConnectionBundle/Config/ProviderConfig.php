<?php
/**
 * Created by PhpStorm.
 * User: simon.garrick
 * Date: 08/11/2017
 * Time: 09:27
 */

namespace Epiphany\OAuthConnectionBundle\Config;


interface ProviderConfig
{

    public function getName():string;

    public function getType():string;

    public function getClientId():string;

    public function getClientSecret():string;

    public function getAuthorizeUrl():string;

    public function getTokenUrl():string;

    public function getResourceOwnerUrl():string;

    public function getScopes():array;

}