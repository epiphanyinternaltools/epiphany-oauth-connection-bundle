<?php
/**
 * Created by PhpStorm.
 * User: simon.garrick
 * Date: 08/11/2017
 * Time: 10:15
 */

namespace Epiphany\OAuthConnectionBundle\Config;


class BasicProviderConfig implements ProviderConfig
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $type;

    /**
     * @var string
     */
    private $clientId;

    /**
     * @var string
     */
    private $clientSecret;

    /**
     * @var string
     */
    private $authorizeUrl;

    /**
     * @var string
     */
    private $tokenUrl;

    /**
     * @var string
     */
    private $resourceOwnerUrl;

    /** @var  array */
    private $scopes;

    /**
     * BasicProviderConfig constructor.
     * @param string $name
     * @param string $type
     * @param string $clientId
     * @param string $clientSecret
     * @param string $authorizeUrl
     * @param string $tokenUrl
     * @param string $resourceOwnerUrl
     * @param array $scopes
     */
    public function __construct($name, $type, $clientId, $clientSecret, $authorizeUrl, $tokenUrl, $resourceOwnerUrl, array $scopes)
    {
        $this->name = $name;
        $this->type = $type;
        $this->clientId = $clientId;
        $this->clientSecret = $clientSecret;
        $this->authorizeUrl = $authorizeUrl;
        $this->tokenUrl = $tokenUrl;
        $this->resourceOwnerUrl = $resourceOwnerUrl;
        $this->scopes = $scopes;
    }


    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getClientId(): string
    {
        return $this->clientId;
    }

    /**
     * @param string $clientId
     */
    public function setClientId(string $clientId)
    {
        $this->clientId = $clientId;
    }

    /**
     * @return string
     */
    public function getClientSecret(): string
    {
        return $this->clientSecret;
    }

    /**
     * @param string $clientSecret
     */
    public function setClientSecret(string $clientSecret)
    {
        $this->clientSecret = $clientSecret;
    }

    /**
     * @return string
     */
    public function getAuthorizeUrl(): string
    {
        return $this->authorizeUrl;
    }

    /**
     * @param string $authorizeUrl
     */
    public function setAuthorizeUrl(string $authorizeUrl)
    {
        $this->authorizeUrl = $authorizeUrl;
    }

    /**
     * @return string
     */
    public function getTokenUrl(): string
    {
        return $this->tokenUrl;
    }

    /**
     * @param string $tokenUrl
     */
    public function setTokenUrl(string $tokenUrl)
    {
        $this->tokenUrl = $tokenUrl;
    }

    /**
     * @return string
     */
    public function getResourceOwnerUrl(): string
    {
        return $this->resourceOwnerUrl;
    }

    /**
     * @param string $resourceOwnerUrl
     */
    public function setResourceOwnerUrl(string $resourceOwnerUrl)
    {
        $this->resourceOwnerUrl = $resourceOwnerUrl;
    }

    /**
     * @return array
     */
    public function getScopes(): array
    {
        return $this->scopes;
    }

    /**
     * @param array $scopes
     */
    public function setScopes(array $scopes)
    {
        $this->scopes = $scopes;
    }



}