<?php
/**
 * Created by PhpStorm.
 * User: simon.garrick
 * Date: 08/11/2017
 * Time: 13:53
 */

namespace Epiphany\OAuthConnectionBundle\Config\Exception;


class ProviderConfigNotFoundException extends \Exception
{

}