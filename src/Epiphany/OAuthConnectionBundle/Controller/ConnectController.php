<?php

namespace Epiphany\OAuthConnectionBundle\Controller;

use Epiphany\OAuthConnectionBundle\Account\AccountStorage;
use Epiphany\OAuthConnectionBundle\Connect\ConnectService;
use Epiphany\OAuthConnectionBundle\Connect\StateStorage;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class ConnectController extends AbstractController
{
    public function __construct(private AccountStorage $accountStorage,
                                private ConnectService $connectService,
                                private StateStorage $stateStorage,
                                private LoggerInterface $logger) {}
    /**
     * @param string $key
     * @Route("/initiate/{key}", name="epiphany_oauth_connect.initiate")
     * @return RedirectResponse|NotFoundHttpException
     */
    public function initiateAction(string $key)
    {
        $account = $this->accountStorage->getAccount($key);

        if ($account === null)
            return $this->createNotFoundException('The Account cannot be found');


        $authUrl = $this->connectService->getAuthorizationUrl($account);
        return $this->redirect($authUrl);
    }

    /**
     * @param Request $request
     * @Route("/connect", name="epiphany_oauth_connect.authorize")
     * @return RedirectResponse|NotFoundHttpException
     */
    public function authorizeAction(Request $request)
    {
        $state = $request->query
            ->get('state')
        ;

        $code = $request->query
            ->get('code')
        ;

        $error = $request->query
            ->get('error')
        ;

        $key = $this->stateStorage->getState($state);

        if ($key === null)
            return $this->createNotFoundException('The Account cannot be found');

        $account = $this->accountStorage->getAccount($key);

        if ($account === null)
            return $this->createNotFoundException('The Account cannot be found');

        if($error) {

            // oauth error - log and redirect without storing a token
            $this->logger
                ->notice(sprintf('The OAuth service returned an error: %s', $error));
            $redirectUrl = $account->getRedirectUrl();

            return $this->redirect($redirectUrl);
        }

        $refreshToken = $this->connectService->getRefreshToken($account, $code);

        $account->setRefreshToken($refreshToken);
        $this->accountStorage->storeAccount($account);

        $redirectUrl = $account->getRedirectUrl();

        return $this->redirect($redirectUrl);

    }
}
