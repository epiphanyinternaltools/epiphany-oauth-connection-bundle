<?php
/**
 * Created by PhpStorm.
 * User: simon.garrick
 * Date: 10/11/2017
 * Time: 15:19
 */

namespace Epiphany\OAuthConnectionBundle\Connect\Exception;


class StateNotFoundException extends \Exception
{

}