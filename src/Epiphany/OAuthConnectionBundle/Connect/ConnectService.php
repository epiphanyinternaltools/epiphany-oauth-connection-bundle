<?php

namespace Epiphany\OAuthConnectionBundle\Connect;

use Epiphany\OAuthConnectionBundle\Account\Account;
use Epiphany\OAuthConnectionBundle\Config\ProviderConfigService;
use Epiphany\OAuthConnectionBundle\Provider\ProviderFactory;

class ConnectService
{
    /**
     * @var ProviderConfigService
     */
    private $configService;

    /** @var  ProviderFactory */
    private $factory;

    /**
     * @var StateStorage
     */
    private $stateStorage;

    public function __construct(
        /** ConfigService */ $configService,
        /** ProviderFactory */ $factory,
        /** StateStorage */ $stateStorage
    )
    {
        $this->stateStorage = $stateStorage;
        $this->factory = $factory;
        $this->configService = $configService;
    }

    public function getAuthorizationUrl(Account $account)
    {
        $accountKey = $account->getKey();
        $accountType = $account->getAccountType();

        $providerConfig = $this->configService
            ->getProviderConfig($accountType);

        $provider = $this->factory
            ->getProvider($providerConfig)
        ;

        $authUrl = $provider->getAuthorizationUrl([
            'approval_prompt' => 'force',
            'scope' => $providerConfig->getScopes()
        ]);

        $state = $provider->getState();
        $this->stateStorage
            ->storeState($state, $accountKey)
        ;

        return $authUrl;
    }

    public function getRefreshToken(Account $account, $code)
    {
        $accountType = $account->getAccountType();

        $providerConfig = $this->configService
            ->getProviderConfig($accountType)
        ;

        $provider = $this->factory->getProvider($providerConfig);

        $accessToken = $provider->getAccessToken(
            'authorization_code',
            ['code' => $code]
        );

        $refreshToken = $accessToken->getRefreshToken() ?? $accessToken->getToken();
        return $refreshToken;
    }

    public function getAccessToken(Account $account)
    {
        $accountRefreshToken = $account->getRefreshToken();
        if($accountRefreshToken === '')
            return null;

        $accountType = $account->getAccountType();

        $providerConfig = $this->configService
            ->getProviderConfig($accountType)
        ;

        $provider = $this->factory->getProvider($providerConfig);

        $accessToken = $provider->getAccessToken(
            'refresh_token',
                ['refresh_token' => $accountRefreshToken]
            );
        return $accessToken;


    }
}
