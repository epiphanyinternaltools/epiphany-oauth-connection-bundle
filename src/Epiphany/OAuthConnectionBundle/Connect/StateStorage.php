<?php

namespace Epiphany\OAuthConnectionBundle\Connect;

use Epiphany\OAuthConnectionBundle\Connect\Exception\StateNotFoundException;
use Symfony\Component\HttpFoundation\Session\Session;

class StateStorage
{
    /**
     * @var Session
     */
    private $session;

    public function __construct(Session $session)
    {
        $this->session = $session;
    }

    public function storeState($state, $accountKey)
    {
        $this->session->set($state, $accountKey);
    }

    public function getState($state)
    {
        if($this->session->has($state) !== true)
        {
            throw new StateNotFoundException();
        }
        return $this->session->get($state);
    }
}
