<?php

namespace Epiphany\OAuthConnectionBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('epiphany_oauth_connection');
        $rootNode = $treeBuilder->getRootNode();

        $rootNode
            ->children()
                ->scalarNode('account_storage')
                ->cannotBeEmpty()
                ->end()
                ->arrayNode('providers')
                    ->arrayPrototype()
                        ->children()
                            ->scalarNode('type')->end()
                            ->scalarNode('client_id')->end()
                            ->scalarNode('client_secret')->end()
                            ->scalarNode('authorize_url')->end()
                            ->scalarNode('token_url')->end()
                            ->scalarNode('resource_owner_url')->end()
                                ->arrayNode('scopes')
                                    ->scalarPrototype()->end()
                                ->end()// arrayNode Scope
                        ->end() // child
                    ->end() // ArrayPrototype (eg Google, Bing ect
                ->end() //Array Node Providers
            ->end() // child
        ;

        return $treeBuilder;
    }

}
