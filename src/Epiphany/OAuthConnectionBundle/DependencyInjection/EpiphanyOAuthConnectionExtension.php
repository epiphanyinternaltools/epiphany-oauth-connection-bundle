<?php

namespace Epiphany\OAuthConnectionBundle\DependencyInjection;

use Epiphany\OAuthConnectionBundle\Account\AccountStorage;
use Epiphany\OAuthConnectionBundle\Config\BasicProviderConfig;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\Config\FileLocator;
/**
 * This is the class that loads and manages your bundle configuration.
 *
 * @link http://symfony.com/doc/current/cookbook/bundles/extension.html
 */
class EpiphanyOAuthConnectionExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');

        $container->setAlias(AccountStorage::class, $config['account_storage']);

        $providerConfigServiceDefinition = $container->getDefinition('config_service');

        foreach($config['providers'] as $name => $provider) {

            $providerConfigDefinition = new Definition(BasicProviderConfig::class, [
                $name,
                $provider['type'],
                $provider['client_id'],
                $provider['client_secret'],
                $provider['authorize_url'],
                $provider['token_url'],
                $provider['resource_owner_url'],
                $provider['scopes']
            ]);

            $providerConfigDefinition->addTag("epiphany_oauth.config_provider");
            $container->setDefinition("epiphany_oauth.config_provider.$name", $providerConfigDefinition);
        }

    }
    public function getAlias()
    {
        return 'epiphany_oauth_connection';
    }
}
