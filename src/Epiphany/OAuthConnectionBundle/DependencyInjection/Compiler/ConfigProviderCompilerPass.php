<?php
/**
 * Created by PhpStorm.
 * User: simon.garrick
 * Date: 20/11/2017
 * Time: 16:56
 */

namespace Epiphany\OAuthConnectionBundle\DependencyInjection\Compiler;


use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class ConfigProviderCompilerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        if (!$container->has('config_service')) {
            return;
        }

        $definition = $container->findDefinition(
            'config_service'
        );


        $taggedServices = $container->findTaggedServiceIds(
            'epiphany_oauth.config_provider'
        );

        foreach ($taggedServices as $id => $tags) {
            $definition->addMethodCall(
                'registerProviderConfig',
                array(new Reference($id))
            );
        }
    }

}