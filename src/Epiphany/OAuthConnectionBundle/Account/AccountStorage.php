<?php
/**
 * Created by PhpStorm.
 * User: simon.garrick
 * Date: 08/11/2017
 * Time: 09:26
 */

namespace Epiphany\OAuthConnectionBundle\Account;


interface AccountStorage
{

    public function getAccount(string $key): Account;

    public function storeAccount(Account $account);

}