<?php
/**
 * Created by PhpStorm.
 * User: simon.garrick
 * Date: 08/11/2017
 * Time: 09:25
 */

namespace Epiphany\OAuthConnectionBundle\Account;


interface Account
{

    public function getKey():string;

    public function getAccountType():string;

    public function getRefreshToken():string;

    public function setRefreshToken(string $refreshToken);

    public function getRedirectUrl(): string;

}