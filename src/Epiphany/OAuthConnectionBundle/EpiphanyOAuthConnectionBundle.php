<?php

namespace Epiphany\OAuthConnectionBundle;

use Epiphany\OAuthConnectionBundle\DependencyInjection\EpiphanyOAuthConnectionExtension;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Epiphany\OAuthConnectionBundle\DependencyInjection\Compiler\ConfigProviderCompilerPass;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class EpiphanyOAuthConnectionBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        parent::build($container);
        $container->addCompilerPass(new ConfigProviderCompilerPass());
    }
    public function getContainerExtension()
    {
        return new EpiphanyOAuthConnectionExtension();
    }
}
