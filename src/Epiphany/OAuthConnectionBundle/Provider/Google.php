<?php
/**
 * Created by PhpStorm.
 * User: dan.martin
 * Date: 22/02/2018
 * Time: 15:12
 */

namespace Epiphany\OAuthConnectionBundle\Provider;

use League\OAuth2\Client\Provider\Google as Base;
use League\OAuth2\Client\Grant\RefreshToken;

/**
 * For some reason Google's oauth sometimes rejects refresh_token grant requests if
 * the redirect_uri parameter is supplied
 * so here we replace the League's Google provider with an extended version
 * that removes the redirect_uri parameter if we're doing refresh_token grant
 */
class Google extends Base
{
    public function getAccessToken($grant, array $options = [])
    {
        $grant = $this->verifyGrant($grant);

        $params = [
            'client_id'     => $this->clientId,
            'client_secret' => $this->clientSecret,
            'redirect_uri'  => $this->redirectUri,
        ];

        if($grant instanceof RefreshToken) {
            unset($params['redirect_uri']);
        }

        $params   = $grant->prepareRequestParameters($params, $options);
        $request  = $this->getAccessTokenRequest($params);
        $response = $this->getParsedResponse($request);
        $prepared = $this->prepareAccessTokenResponse($response);
        $token    = $this->createAccessToken($prepared, $grant);

        return $token;
    }

}