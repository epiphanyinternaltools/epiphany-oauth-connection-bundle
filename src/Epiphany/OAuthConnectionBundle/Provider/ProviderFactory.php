<?php
/**
 * Created by PhpStorm.
 * User: simon.garrick
 * Date: 08/11/2017
 * Time: 16:47
 */

namespace Epiphany\OAuthConnectionBundle\Provider;

use Epiphany\OAuthConnectionBundle\Config\ProviderConfig;
use League\OAuth2\Client\Provider\AbstractProvider;
use League\OAuth2\Client\Provider\GenericProvider;
use Symfony\Component\Routing\Router;
use TheNetworg\OAuth2\Client\Provider\Azure;


class ProviderFactory
{
    /**
     * @var Router
     */
    private $router;

    public function __construct(Router $router)
    {
        $this->router = $router;
    }

    public function getProvider(ProviderConfig $providerConfig): AbstractProvider
    {

        $redirectUrl = $this->router->generate('epiphany_oauth_connect.authorize', [], Router::ABSOLUTE_URL);

        switch ($providerConfig->getType())
        {
            case "google":
                $provider = new Google([
                    'clientId' => $providerConfig->getClientId(),
                    'clientSecret' => $providerConfig->getClientSecret(),
                    'redirectUri' => $redirectUrl,
                    'accessType' => 'offline',
                    'scopes' => $providerConfig->getScopes()
                ]);
                break;
            case "azure":
                $provider = new Azure([
                    'clientId' => $providerConfig->getClientId(),
                    'clientSecret' => $providerConfig->getClientSecret(),
                    'redirectUri' => $redirectUrl,
                    'scopes' => $providerConfig->getScopes()
                ]);
                break;
            default:
                $provider = new GenericProvider([
                    'clientId' => $providerConfig->getClientId(),
                    'clientSecret' => $providerConfig->getClientSecret(),
                    'redirectUri' => $redirectUrl,
                    'urlAuthorize' => $providerConfig->getAuthorizeUrl(),
                    'urlAccessToken' => $providerConfig->getTokenUrl(),
                    'urlResourceOwnerDetails' => $providerConfig->getResourceOwnerUrl(),
                    'scopes' => $providerConfig->getScopes(),
                    'accessType' => 'offline'
                ]);
        }

        return $provider;

    }
}