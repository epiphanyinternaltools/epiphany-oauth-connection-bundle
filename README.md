Epiphany OAuthConnectionBundle
=======================

Lightweight Symfony bundle/wrapper for allowing access to google/azure

Configuration
-------------

```
composer require epiphany/oauth-connect-bundle
```

Add to AppKernel
```
...new Epiphany\OAuthConnectionBundle\EpiphanyOAuthConnectionBundle()...
```

Add to Routing
```
    oauth_connect_bundle:
            resource: "@EpiphanyOAuthConnectionBundle/Controller/ConnectController.php"
            type: annotation
            prefix: /oauth
```

Add to config.yml
```
epiphany_oauth_connection:
    account_storage: ~
    providers:
        google:
            client_id: ~
            client_secret: ~
            authorize_url: ~
            token_url: ~
            resource_owner_url: ~
            scopes: [~]

```
Google could be changed to a different value. for example Azure

To Do
-------------

```

 - More Account types (bing ect)

```
